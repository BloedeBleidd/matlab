function [ letter, signs, im ] = readImage( picture, width, height, threshold )  
    if( ~exist( 'threshold', 'var' ) )
        threshold = 0.5;
    end

    im = ~im2bw( imread( picture ), threshold );
    signs = regionprops( bwlabel(im), 'all' );
    letter = zeros( length( signs ), height*width );

    for n = 1:length( signs )
        tmp = imresize( signs(n).Image, [height width] );
        letter(n,:) = tmp(:);
    end
    
    signs = length( signs );
end