classdef neuralLetterRecogniser < handle
    properties
        weight;
        neurons = 26;
    end
    
    methods
        function obj = neuralLetterRecogniser( inputs, range )
            if( ~exist( 'range', 'var' ) )
                range = 1;
            end

            obj.weight = rand( inputs+1 , obj.neurons );
            obj.weight = range*( obj.weight - 0.5 );
        end
        
        function outputs = result( obj, inputs, beta )
            if( ~exist( 'beta', 'var' ) )
                beta = 1;
            end

            w = obj.weight';
            tmp = w*[ -1 ; inputs ];
            outputs = ( exp( -tmp*beta ) + 1 ) ;
            outputs = outputs.^-1;
        end
        
        function a = resultAscii( obj, inputs, beta )
            if( ~exist( 'beta', 'var' ) )
                beta = 1;
            end
            
            r = obj.result( inputs, beta );
            [M,I] = max( r );
            a = I+64;
            a = char(a);
        end
        
        function pro = asciiToProper( obj, c )
            pro = zeros( obj.neurons, length(c) );
            
            for k = 1:length(c)
                pro( double(c(k)) - 64, k ) = 1;
            end
        end
        
        function learn( obj, examples, results, n, alfa, beta )
            if( ~exist( 'alfa', 'var' ) )
                alfa = 0.1;
            end
            
            if( ~exist( 'beta', 'var' ) )
                beta = 0.1;
            end

            for k = 1:n
                randomExampleIndex = randi( [ 1, size( examples, 2 ) ] );
                inputs = examples( : , randomExampleIndex );
                outputs = obj.result( inputs, beta );
                delta = results( : , randomExampleIndex ) - outputs;
                delta = delta';
                obj.weight = obj.weight + [ -1 ; inputs ]*delta*alfa;
            end
        end
        
        function setWeights( obj, weights )
            obj.weight = weights;
        end
        
        function w = getWeights( obj )
            w = obj.weight;
        end
        
        function setBias( obj, bias )
            obj.bias = bias;
        end
        
        function s = getInputSize( obj )
            s = length( obj.weight(:,1) );
        end
        
        function s = getLayerSize( obj )
            s = length( obj.weight(1,:) );
        end
    end
end



